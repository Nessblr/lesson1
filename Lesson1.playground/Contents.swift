import UIKit

let numberValue = 5
let boolValue = true
let floatValue = 5.33333
let doubeValue: Double = 5.33333

var str = "Hello, playground"


if str == "Hello, playground" {
    print("yes")
} else if str == "kek"{
    print("no")
} else {
    print("wait")
}

switch str {
case "str":
    print("yes")
default:
    print("lol")
}

print("=========================HOME WORK=================================================") 
var example = "beauty"

for i in example {
    
    let i = String(i)
    switch i {
     case "a","e","u","y": print(i.uppercased())
     
     default: print(i)
          
}

}

print("======================")  


let str2 = "full"

if example.count>3 {
    
  print(example + str2)
  
}

print("=======ЗДЕСЬ ФУНКЦИЯ===============")  

func convertStr(text: String) -> String  {
   
    var newText = ""
    
    for i in text {
    let i = String(i)
    switch i {
    
        case "q","w","r","t","p","s","d","f","g","h","j",
        "k","l","z","x","c","v","b","n","m": 
        
            newText += i
            
        case "a","e","i","o","u","y":
        
            newText += i.uppercased()
        
      
        
        default: newText += " "
    }
}
    
    return newText
}

let mainText = "i called you to ask for help"
let newText2 = convertStr(text: mainText)

print(newText2)





//  Task1  

//  Напишите функцию в которую будет поступать строка в качестве параметра String, подсчитайте кол-во букв в строке и умножте на число которое будет находится вне функции, результат вернуть сфункции и разделить на 2




var number = 3

func countString(str: String) -> Int  {
   
    let newStr = str.count * number
    
    return newStr / 2 
}


let mainText = "example"

let newText = countString(str: mainText)

print(newText)

print("==================================")

// Task 2

// Создайте массив кортежей (тюплов), в которых будет первое поле имя студента, второе поле возраст студента, третье поле его оценка (от 0 до 10). Заполните 20 значениями.



var firstName: [String] = ["Jon", "Pavel", "Rustam", "Artem", "Jora", "Alex", "Max", "German", "Tima"]
print("firstName = \(firstName)")

var age: [Int] = [11, 22, 33, 44, 55, 66, 77, 88, 99, 100]
print("Age = \(age)")

var rating: [Int] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print("rating = \(rating)")

var tupleArray = [(firstname: "Jon", age: 20, rating: 10)]

print("tupleArray = \(tupleArray)")



var array: [String] = ["qwe", "ww"]

array[1]